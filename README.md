# webbench

#### 介绍
http://home.tiscali.cz/~cz210552/
Radim Kolar
Web Bench 1.5 - Updated Jul 13 2004/ Jun 25 2004


Webbench是一个在 Linux 下使用的非常简单的网站压测工具。

它使用fork()模拟多个客户端同时访问我们设定的URL，测试网站在压力下工作的性能。

最多可以模拟 3 万个并发连接去测试网站的负载能力。

Webbench使用C语言编写， 代码实在太简洁，源码加起来不到 600 行。